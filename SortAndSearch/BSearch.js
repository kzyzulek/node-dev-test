"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BSearch = /** @class */ (function () {
    function BSearch() {
        this._operations = 0; // Number of operations initial value
    }
    BSearch.prototype.operations = function () {
        return this._operations;
    };
    BSearch.prototype.recursiveSearch = function (arr, x, start, end) {
        // Base Condtion 
        if (start > end)
            return -1;
        // Find the middle index w
        var mid = Math.floor((start + end) / 2);
        // Compare mid with given key x 
        if (arr[mid] === x)
            return mid;
        // If element at mid is greater than x, 
        // search in the left half of mid 
        if (arr[mid] > x)
            return this.recursiveSearch(arr, x, start, mid - 1);
        else
            // If element at mid is smaller than x, 
            // search in the right half of mid 
            return this.recursiveSearch(arr, x, mid + 1, end);
    };
    BSearch.prototype.Search = function (element, sortedTable) {
        this._operations++;
        var index = this.recursiveSearch(sortedTable, element, 0, sortedTable.length);
        return index;
    };
    return BSearch;
}());
exports.BSearch = BSearch;
