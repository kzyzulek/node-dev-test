// npm install -g typescript

// https://stackoverflow.com/questions/33535879/how-to-run-typescript-files-from-command-line
// tsc main.ts | node main.js       Windows
// tsc main.ts && node main.js      Linux



import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

// Input values
const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

console.log('Input values');
console.log(unsorted);
console.log(elementsToFind);


// ASort class test
let asort = new ASort();
let sorted = asort.Sort(unsorted);
console.log('ASort');
console.log(sorted);


// BSort class test
let bsort = new BSort();
sorted = bsort.Sort(unsorted);
console.log('BSort');
console.log(sorted);


// BSearch
console.log('BSearch');
let bsearch = new BSearch();

let elementsToFindLength = elementsToFind.length;
for(let i=0; i<elementsToFindLength; i++) {
    let index = bsearch.Search(elementsToFind[i], sorted);
    console.log(bsearch.operations(), elementsToFind[i], index);
}


