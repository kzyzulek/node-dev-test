"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BSort = /** @class */ (function () {
    function BSort() {
    }
    // Quick sort
    BSort.prototype.swap = function (arr, leftIndex, rightIndex) {
        var temp = arr[leftIndex];
        arr[leftIndex] = arr[rightIndex];
        arr[rightIndex] = temp;
    };
    BSort.prototype.partition = function (arr, left, right) {
        var pivot = arr[Math.floor(((right + left) * 1) / 2)], // middle element
        i = left, // left pointer
        j = right; // right pointer
        while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                this.swap(arr, i, j); // sawpping two elements
                i++;
                j--;
            }
        }
        return i;
    };
    BSort.prototype.quickSort = function (arr, left, right) {
        var index;
        if (arr.length > 1) {
            index = this.partition(arr, left, right); // index returned from partition
            if (left < index - 1) { // more elements on the left side of the pivot
                this.quickSort(arr, left, index - 1);
            }
            if (index < right) { // more elements on the right side of the pivot
                this.quickSort(arr, index, right);
            }
        }
        return arr;
    };
    BSort.prototype.Sort = function (arr) {
        return this.quickSort(arr, 0, arr.length - 1);
        //return table.sort((n1,n2) => n1 - n2);
    };
    return BSort;
}());
exports.BSort = BSort;
