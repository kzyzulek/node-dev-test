export class BSearch {
    private static instance: BSearch;           // Singleton instance BSearch

    constructor() {
        this._operations = 0;                   // Number of operations initial value
    }
    

    _operations: number;                        // Number of operations
    operations() {                              // Number of operations Getter
        return this._operations;
    }


    recursiveSearch(arr, x, start, end) { 
        // Base Condtion 
        if (start > end) return -1; 
       
        // Find the middle index w
        let mid=Math.floor((start + end)/2); 
       
        // Compare mid with given key x 
        if (arr[mid]===x) return mid; 
              
        // If element at mid is greater than x, 
        // search in the left half of mid 
        if(arr[mid] > x)  
            return this.recursiveSearch(arr, x, start, mid-1); 
        else
            // If element at mid is smaller than x, 
            // search in the right half of mid 
            return this.recursiveSearch(arr, x, mid+1, end); 
    }
    
    
    Search(element: number, sortedTable) {
        this._operations++;

        let index = this.recursiveSearch(sortedTable, element, 0, sortedTable.length);

        return index;
    }
}