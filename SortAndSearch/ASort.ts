export class ASort {

    // Merge sort
    
    Merge(left, right)
    {
        let result = [];
    
        while (left.length && right.length) {
            if (left[0] <= right[0]) {
                result.push(left.shift());
            } else {
                result.push(right.shift());
            }
        }
    
        while (left.length)
            result.push(left.shift());
    
        while (right.length)
            result.push(right.shift());
    
        return result;
    }
    
    
    Sort(arr) {
        if (arr.length < 2)
        return arr;

        // Divide array 
        let middle: number = arr.length / 2;
        let left   = arr.slice(0, middle);
        let right  = arr.slice(middle, arr.length);

        // Merge left and right side arrays
        return this.Merge(this.Sort(left), this.Sort(right));
        //return table.sort((n1,n2) => n1 - n2);
    }
}