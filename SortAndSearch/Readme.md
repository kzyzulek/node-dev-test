## Uruchamianie w konsoli bash

clear && tsc --target es5 index.ts && node index.js


## ASort
# Merge sort
https://stackoverflow.com/questions/1427608/fast-stable-sorting-algorithm-implementation-in-javascript

## BSort
# Quick sort
https://www.guru99.com/quicksort-in-javascript.html

## BSearch
# Binary search
https://www.geeksforgeeks.org/binary-search-in-javascript/

