"use strict";
// npm install -g typescript
Object.defineProperty(exports, "__esModule", { value: true });
// https://stackoverflow.com/questions/33535879/how-to-run-typescript-files-from-command-line
// tsc main.ts | node main.js       Windows
// tsc main.ts && node main.js      Linux
var ASort_1 = require("./ASort");
var BSort_1 = require("./BSort");
var BSearch_1 = require("./BSearch");
// Input values
var unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
var elementsToFind = [1, 5, 13, 27, 77];
console.log('Input values');
console.log(unsorted);
console.log(elementsToFind);
// ASort class test
var asort = new ASort_1.ASort();
var sorted = asort.Sort(unsorted);
console.log('ASort');
console.log(sorted);
// BSort class test
var bsort = new BSort_1.BSort();
sorted = bsort.Sort(unsorted);
console.log('BSort');
console.log(sorted);
// BSearch
console.log('BSearch');
var bsearch = new BSearch_1.BSearch();
var elementsToFindLength = elementsToFind.length;
for (var i = 0; i < elementsToFindLength; i++) {
    var index = bsearch.Search(elementsToFind[i], sorted);
    console.log(bsearch.operations(), elementsToFind[i], index);
}
