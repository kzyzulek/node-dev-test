"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ASort = /** @class */ (function () {
    function ASort() {
    }
    // Merge sort
    ASort.prototype.Merge = function (left, right) {
        var result = [];
        while (left.length && right.length) {
            if (left[0] <= right[0]) {
                result.push(left.shift());
            }
            else {
                result.push(right.shift());
            }
        }
        while (left.length)
            result.push(left.shift());
        while (right.length)
            result.push(right.shift());
        return result;
    };
    ASort.prototype.Sort = function (arr) {
        if (arr.length < 2)
            return arr;
        // Divide array 
        var middle = arr.length / 2;
        var left = arr.slice(0, middle);
        var right = arr.slice(middle, arr.length);
        // Merge left and right side arrays
        return this.Merge(this.Sort(left), this.Sort(right));
        //return table.sort((n1,n2) => n1 - n2);
    };
    return ASort;
}());
exports.ASort = ASort;
