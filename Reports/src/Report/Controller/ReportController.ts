import { Controller, Get, Param, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestBuyers, IBestSellers } from '../Model/IReports'

@Controller()
export class ReportController {
  @Inject() orderMapper: OrderMapper;


  @Get("/report/products/:date")
  async bestSellers(@Param("date") date: string): Promise<any> {

    async function asyncAwaitFunction(orderMapper): Promise<any> {      
      let mappedOrders = await orderMapper.mappedOrders();
      
      let responseData: IBestSellers[] = [];
      
      for( let i=0; i<mappedOrders.length; i++) {
        if(mappedOrders[i].createdAt == date) {
          let exist = false;
          let j = 0;
          for( j = 0; j<responseData.length; j++) {
            if (responseData[j].productName == mappedOrders[i].productName) {
              exist = true;
              break;
            }
          }

          if(! exist) {
            responseData.push({
              productName: mappedOrders[i].productName,
              quantity: 1,
              totalPrice: mappedOrders[i].productPrice
            })
          } else {
            responseData[j].quantity++;
            responseData[j].totalPrice += mappedOrders[i].productPrice;
          }
        }
      }
      
      return responseData
    }

    return asyncAwaitFunction(this.orderMapper);
  }






  @Get("/report/customer/:date")
  bestBuyers(@Param("date") date: string) {
    async function asyncAwaitFunction(orderMapper): Promise<any> {      
      let mappedOrders = await orderMapper.mappedOrders();
      
      let responseData: IBestBuyers[] = [];
      
      for( let i=0; i<mappedOrders.length; i++) {
        if(mappedOrders[i].createdAt == date) {
          let exist = false;
          let j = 0;
          for( j = 0; j<responseData.length; j++) {
            if (responseData[j].customerName == 
                  mappedOrders[i].customerFirstName + ' ' + mappedOrders[i].customerLastName) {
              exist = true;
              break;
            }
          }

          if(! exist) {
            responseData.push({
              customerName: mappedOrders[i].customerFirstName + ' ' + mappedOrders[i].customerLastName,
              totalPrice: mappedOrders[i].productPrice
            })
          } else {
            responseData[j].totalPrice += mappedOrders[i].productPrice;
          }
        }
      }

      // Search for Best Buyer
      let maxI = -1;
      let maxValue = 0;

      for(let i = 0; i<responseData.length; i++) {
        if(responseData[i].totalPrice > maxValue) {
          maxI = i;
          maxValue = responseData[i].totalPrice;
        }
      }
      
      return responseData[maxI]
    }

    return asyncAwaitFunction(this.orderMapper);
  }
}
