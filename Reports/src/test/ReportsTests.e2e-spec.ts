import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from '../Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET) - 404', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('/report/products (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-07')
      .expect(200).expect([
        {
        "productName": "Black sport shoes",
        "quantity": 2,
        "totalPrice": 220
        },
        {
        "productName": "Cotton t-shirt XL",
        "quantity": 1,
        "totalPrice": 25.75
        }
        ]);
  });

  it('/report/customer (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07')
      .expect(200).expect({
        "customerName": "John Doe",
        "totalPrice": 135.75
        });
  });
});
