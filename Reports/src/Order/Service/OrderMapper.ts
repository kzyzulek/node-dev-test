import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { Customer } from '../Model/Customer'
import { Product } from '../Model/Product'
import { iif } from 'rxjs';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;


  async mappedOrders() {
    let mappedOrders: Order[] = [];

    
    this.repository.fetchCustomers().then(
      (customers: Customer[]) => {
        this.repository.fetchProducts().then(
          (products: Product[]) => {
            this.repository.fetchOrders().then(
              (orders: Order[]) => {
              for(var i = 0; i < orders.length; i++) {
                
                let order : Order = orders[i];
                let customer : Customer;

                for(let k = 0; k < customers.length; k++) {
                  if( customers[k].id == orders[i].customer) {
                    customer = customers[k]; 
                    break;
                  }
                }


                for(let j = 0; j < orders[i].products.length; j++ ) {

                  let product: Product;
                  for(let k = 0; k < products.length; k++) {
                    if(products[k].id == orders[i].products[j])
                    {
                      product = products[k]; 
                      break;
                    }
                  }

                  mappedOrders.push(Object.assign({}, 
                    order, 
                    { 
                      customerId: customer.id,
                      customerFirstName: customer.firstName,
                      customerLastName: customer.lastName,
                      productId: product.id, 
                      productName: product.name,
                      productPrice: product.price
                     }
                    
                  ));

                  delete mappedOrders[mappedOrders.length - 1].customer;
                  delete mappedOrders[mappedOrders.length - 1].products;                  

                }
              }
            }
            )
          }
        )    
      }
    )

  
    return new Promise(resolve => resolve(mappedOrders));
  
  }

}


